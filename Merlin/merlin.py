import configparser
import argparse

from sys import exc_info
from time import sleep
from os import mkdir
from os import getcwd
from os import path

from tradingbot.robot import Tradebot
from tradingbot.logic.strategy import STR
from tradingbot.utils.logging import Log
from tradingbot.exchange.exchange import Exchange
from tradingbot.exchange.exchange import ConnectionErrorException
from tradingbot.exchange.enums import *
from requests.exceptions import RequestException


def load_config():
    conf = configparser.ConfigParser()
    try:
        conf.read_file(open("data/merlin.conf", "r"))
        config_ok = True

        if 'merlin' in conf:
            try:
                if 'API_KEY' not in conf['merlin'] \
                        or 'API_SECRET' not in conf['merlin'] \
                        or 'SYMBOL_LEFT' not in conf['merlin'] \
                        or 'SYMBOL_RIGHT' not in conf['merlin'] \
                        or 'RISK_TOLERANCE' not in conf['merlin'] \
                        or 'TRADING_PERIOD' not in conf['merlin'] \
                        or 'TEST_MODE' not in conf['merlin']:
                    config_ok = False
                if len(conf['merlin']['API_KEY']) != 64:
                    config_ok = False
                if len(conf['merlin']['API_SECRET']) != 64:
                    config_ok = False
                if len(conf['merlin']['SYMBOL_LEFT']) == 0:
                    config_ok = False
                if len(conf['merlin']['SYMBOL_RIGHT']) == 0:
                    config_ok = False
                if conf['merlin']['TRADING_STRATEGY'] not in [val.value for val in STR]:
                    config_ok = False
                if 0.001 > float(conf['merlin']['RISK_TOLERANCE']) or float(conf['merlin']['RISK_TOLERANCE']) > 1.0:
                    config_ok = False
                if int(conf['merlin']['TRADING_PERIOD']) < 50:
                    config_ok = False
                if conf['merlin']['TEST_MODE'] not in ['True', 'False']:
                    config_ok = False

            except KeyError:
                config_ok = False

            if not config_ok:
                raise Exception("Invalid config, check the config file.")
            else:
                return conf['merlin']

    except FileNotFoundError:
        if not path.exists(path.join(getcwd(), 'data')):
            mkdir("data")

        conf['merlin'] = {
            'API_KEY': "",
            'API_SECRET': "",
            'SYMBOL_LEFT': "",
            'SYMBOL_RIGHT': "",
            'TRADING_STRATEGY': "MACD",
            'RISK_TOLERANCE': 0.01,
            'TRADING_PERIOD': 200,
            'TEST_MODE': False
        }
        with open("./data/merlin.conf", "w") as conffile:
            conf.write(conffile)
            raise Exception("No proper config found, check the config file.")


if __name__ == '__main__':

    # Arguments parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--simulate", type=int,
                        help="simulate trading session n-times and print back stats")
    parser.add_argument("--no-chart", action="store_true",
                        help="Disable chart display after trading simulation")
    parser.add_argument("-l", "--live", action="store_true",
                        help="simulate live trading session and print back stats")

    args = parser.parse_args()

    try:
        # Load configuration
        config = load_config()

        test_mode = True if config['TEST_MODE'] == "True" else False

        bot: Tradebot = Tradebot(Exchange(EXCHG_BINANCE, (config['SYMBOL_LEFT'], config['SYMBOL_RIGHT']),
                                          api_key=config['API_KEY'], api_secret=config['API_SECRET']),
                                 (config['SYMBOL_LEFT'], config['SYMBOL_RIGHT']),
                                 float(config['RISK_TOLERANCE']),
                                 config['TRADING_STRATEGY'],
                                 int(config['TRADING_PERIOD']),
                                 test_mode=test_mode)

        if args.simulate:
            bot.perform_fake_trade_session(args.simulate)
            if not args.no_chart:
                bot.display_price_chart(display_indicators=True)

        else:
            while True:
                try:
                    bot.perform_lap()
                    bot.wait_next_bar()

                except (ConnectionErrorException, ConnectionError, ConnectionResetError, RequestException) as e:
                    Log.error(e)
                    sleep(5)

    except Exception as e:
        exc_type, exc_obj, exc_tb = exc_info()
        fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        Log.error(str(type(e)) + str(e) + '\nTYPE {} IN ({}, LINE {})'.format(exc_type, fname, exc_tb.tb_lineno))
        exit(1)
