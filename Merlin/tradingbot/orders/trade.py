from enum import Enum
from time import time
from datetime import datetime

from tradingbot.orders.enums import *


class Trade:
    side: Enum = None
    trade_type: Enum = None
    price: float = .0
    quantity: float = .0
    date: datetime = 0
    status: Enum = None
    trade_id: int = 0
    closing_price: float = .0
    closed = False
    closing_time: datetime = 0

    def __init__(self, side: Enum, trade_type: Enum, price: float, quantity: float, date: datetime, trade_id: int = 0,
                 stop_price: float = None):
        self.side = side
        self.trade_type = trade_type
        self.price = price
        self.quantity = quantity

        if date is None or date == 0:
            self.date = datetime.utcnow()
        else:
            self.date = date

        self.trade_id = trade_id

        if stop_price is not None:
            self.stop_price = stop_price

        if self.trade_type == TYPE_MARKET:
            self.close(self.price, date)

    def __str__(self):
        res = "side : " + str(self.side) + " - type: " + str(self.trade_type) + " - price: " + str(self.price)
        if hasattr(self, 'stop_price'):
            res += " - stop price: " + str(self.stop_price)
        res += " - quantity: " + str(self.quantity)
        return res

    def close(self, closing_price: float, closing_time: datetime):
        self.closing_price = closing_price
        self.closing_time = closing_time
        self.closed = True
