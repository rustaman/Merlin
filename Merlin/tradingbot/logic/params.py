BUY_SIGNAL_LABEL = "BUY_SIG"
SELL_SIGNAL_LABEL = "SELL_SIG"

MACD_LABEL = "MACD"
MACD_SIGNAL_LABEL = "MACD_SIG"

BOLL_MA = "BOLL_MA"
BOLL_UP = "BOLL_UP"
BOLL_DOWN = "BOLL_DOWN"
BOLL_STD = "BOLL_STD"

STOCHASTIC_K_LABEL = "%K"
STOCHASTIC_D_LABEL = "%D"
STOCHASTIC_LOW_LIMIT_LABEL = "STOCH_LOW_LIMIT"
STOCHASTIC_HIGH_LIMIT_LABEL = "STOCH_HIGH_LIMIT"

RSI_LABEL = "RSI"
RSI70_LABEL = "RSI70"
RSI30_LABEL = "RSI30"


macd_short_span = 15
macd_long_span = 30
macd_signal_span = 12

trend_ema_span = 100

triple_ema_short = 3
triple_ema_mid = 8
triple_ema_long = 20

BOLL_PERIOD = 14
BOLL_THRESHOLD_FACTOR = 2

CANDLESTICK_RATIO_THRESHOLD = 0.

DOUBLE_EMA_LONG = 3
DOUBLE_EMA_SHORT = 2

STOCH_K = 20
STOCH_D = 5
STOCH_SMOOTH = 5

STOCH_LOW = 30.
STOCH_HIGH = 70.

RSI_PERIOD = 50
RSI_UPPER_LIMIT = 80.
RSI_LOWER_LIMIT = 30.
