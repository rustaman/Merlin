import pandas as pd
import numpy as np

from enum import Enum

from tradingbot.logic.indicators import Indicators
from tradingbot.logic.params import *


class STR(Enum):
    MACD = "MACD"
    TRIPLE_EMA = "3EMA"
    BOLL = "BOLL"
    STOCH = "STOCH"


class Strategy:
    _strategy: STR

    def __init__(self, prices: pd.DataFrame, strategy: str):
        self.prices: pd.dataFrame = prices
        for item in STR:
            if item.value == strategy:
                self._strategy = item

    def update_prices(self, prices: pd.DataFrame):
        self.prices = prices

    def append_last_price(self, price):
        if price.iloc[-1, :]['close_time'] != self.last_price['close_time']:
            self.prices = self.prices.append(price, verify_integrity=True)

    @property
    def strategy(self):
        return self._strategy

    @property
    def last_price(self):
        return self.prices.iloc[-1, :]

    def get_buy_sell_signals(self):
        buy = []
        sell = []
        # Indicators.add_ema(self.prices, trend_ema_span)

        if self._strategy == STR.MACD:
            Indicators.add_macd(self.prices)
            Indicators.add_ema(self.prices, trend_ema_span)
            """Strategy without macd above or below zero in order to perform buy and sell signals"""
            for i in range(len(self.prices[MACD_SIGNAL_LABEL])):
                if self.prices[MACD_SIGNAL_LABEL][i] < self.prices[MACD_LABEL][i]:
                    sell.append(np.nan)
                    buy.append(float(self.prices['close'][i]))

                    # if float(self.prices['close'][i]) > float(self.prices['EMA' + str(trend_ema_span)][i]):
                    #     buy.append(float(self.prices['close'][i]))
                    # else:
                    #     buy.append(np.nan)

                elif self.prices[MACD_SIGNAL_LABEL][i] > self.prices[MACD_LABEL][i] > 0.:
                    buy.append(np.nan)
                    sell.append(float(self.prices['close'][i]))

                    # if float(self.prices['close'][i]) < float(self.prices['EMA' + str(trend_ema_span)][i]):
                    #     sell.append(float(self.prices['close'][i]))
                    # else:
                    #     sell.append(np.nan)
                else:
                    buy.append(np.nan)
                    sell.append(np.nan)

        elif self._strategy == STR.TRIPLE_EMA:
            Indicators.add_triple_ema(self.prices, triple_ema_short, triple_ema_mid, triple_ema_long)
            flag_short = False
            flag_long = False

            for i in range(len(self.prices['EMA_SHORT'])):
                # Buy the stock
                if self.prices['EMA_LONG'][i] > self.prices['EMA_MID'][i] > self.prices['EMA_SHORT'][i] \
                        and not flag_long:
                    buy.append(self.prices['close'][i])
                    sell.append(np.nan)
                    flag_short = True
                # Sell stock
                elif flag_short and self.prices['EMA_SHORT'][i] > self.prices['EMA_MID'][i]:
                    sell.append(self.prices['close'][i])
                    buy.append(np.nan)
                    flag_short = False
                # Buy stock
                elif self.prices['EMA_LONG'][i] < self.prices['EMA_MID'][i] < self.prices['EMA_SHORT'][
                    i] and not flag_long:
                    buy.append(self.prices['close'][i])
                    sell.append(np.nan)
                    flag_long = True
                # Sell stock
                elif flag_long and self.prices['EMA_SHORT'][i] < self.prices['EMA_MID'][i]:
                    sell.append(self.prices['close'][i])
                    buy.append(np.nan)
                    flag_long = False
                else:
                    buy.append(np.nan)
                    sell.append(np.nan)

        elif self._strategy == STR.BOLL:
            Indicators.add_bollinger_band(self.prices, BOLL_PERIOD, BOLL_THRESHOLD_FACTOR)
            Indicators.add_stochastic(self.prices, STOCH_K, STOCH_D, STOCH_SMOOTH)

            self.prices["OUTER_LIMIT"] = [np.nan] * len(self.prices[BOLL_MA])

            flag_boll = False
            flag_stoch = False

            for i in range(len(self.prices[BOLL_MA])):

                if self.prices['open'][i] > self.prices['close'][i]:
                    high_mid = self.prices['open'][i]
                    low_mid = self.prices['close'][i]
                else:
                    high_mid = self.prices['close'][i]
                    low_mid = self.prices['open'][i]

                if self.prices['low'][i] < self.prices[BOLL_DOWN][i] < high_mid:

                    buy.append(self.prices['close'][i])
                    sell.append(np.nan)

                elif self.prices['close'][i] >= self.prices[BOLL_UP][i] \
                        or (self.prices[STOCHASTIC_K_LABEL][i] <= self.prices[STOCHASTIC_D_LABEL][i] and
                            not self.prices[STOCHASTIC_K_LABEL][i] < self.prices[STOCHASTIC_HIGH_LIMIT_LABEL][i]):
                    sell.append(self.prices['close'][i])
                    buy.append(np.nan)

                else:
                    buy.append(np.nan)
                    sell.append(np.nan)

                # if self.prices[BOLL_DOWN][i] > self.prices['close'][i]:
                #     buy.append(self.prices['close'][i])
                #     sell.append(np.nan)
                #
                #
                # # elif self.prices[BOLL_UP][i] < self.prices['close'][i]:
                # #     if self.prices[RSI_LABEL][i] > self.prices[RSI70_LABEL][i]:
                # #         sell.append(self.prices['close'][i])
                # #         buy.append(np.nan)
                # #
                # #     else:
                # #         buy.append(np.nan)
                # #         sell.append(np.nan)
                #
                # else:
                #     buy.append(np.nan)
                #     sell.append(np.nan)
                #
                # # if self.prices[BOLL_DOWN][i] > self.prices['close'][i]:
                # #     buy.append(self.prices['close'][i])
                # #     sell.append(np.nan)
                # #
                # # elif self.prices[BOLL_UP][i] < self.prices['close'][i]:
                # #     sell.append(self.prices['close'][i])
                # #     buy.append(np.nan)
                # #
                # # else:
                # #     buy.append(np.nan)
                # #     sell.append(np.nan)

        elif self._strategy == STR.STOCH:
            Indicators.add_stochastic(self.prices, STOCH_K, STOCH_D, STOCH_SMOOTH)
            Indicators.add_ema(self.prices, 25)
            flag_under_limit = False
            flag_signal = True
            for i in range(len(self.prices[STOCHASTIC_K_LABEL])):

                if self.prices[STOCHASTIC_K_LABEL][i] >= self.prices[STOCHASTIC_D_LABEL][i] \
                        and self.prices[STOCHASTIC_LOW_LIMIT_LABEL][i] <= self.prices[STOCHASTIC_K_LABEL][i] \
                        and flag_under_limit:
                    flag_under_limit = False
                    buy.append(self.prices['close'][i])
                    sell.append(np.nan)
                    flag_signal = True

                elif self.prices[STOCHASTIC_LOW_LIMIT_LABEL][i] >= self.prices[STOCHASTIC_K_LABEL][i] and flag_signal:
                    flag_under_limit = True
                    flag_signal = False
                    buy.append(np.nan)
                    sell.append(np.nan)

                elif self.prices[STOCHASTIC_HIGH_LIMIT_LABEL][i] < self.prices[STOCHASTIC_K_LABEL][i] \
                        and not self.prices[STOCHASTIC_K_LABEL][i] > self.prices[STOCHASTIC_D_LABEL][i]:
                    sell.append(self.prices['close'][i])
                    buy.append(np.nan)

                elif self.prices[STOCHASTIC_K_LABEL][i] <= self.prices[STOCHASTIC_D_LABEL][i] \
                        and self.prices[STOCHASTIC_HIGH_LIMIT_LABEL][i] > self.prices[STOCHASTIC_D_LABEL][i] < \
                        self.prices[STOCHASTIC_LOW_LIMIT_LABEL][i]:
                    flag_under_limit = False
                    flag_signal = True
                    buy.append(np.nan)
                    sell.append(np.nan)

                else:
                    buy.append(np.nan)
                    sell.append(np.nan)

        # Add Buy and Sell signals to the data set
        self.prices[BUY_SIGNAL_LABEL] = buy
        self.prices[SELL_SIGNAL_LABEL] = sell

        return self.prices

    def get_trend(self, period):
        Indicators.add_ema(self.prices, period)
        label = 'EMA' + str(period)
        first = self.prices[label][0]
        last = self.prices[label][len(self.prices[label]) - 1]
        mid_idx = int(len(self.prices[label]) / 2)
        mid = self.prices[label][mid_idx]
        if first < mid < last:
            return 1
        elif first > mid and mid < last:
            return 1
        elif first > mid > last:
            return -1
        elif first < mid and mid > last:
            return -1
        else:
            return 0
