import pandas as pd
from numpy import nan

from tradingbot.logic.params import *


class Indicators:
    @staticmethod
    def add_macd(prices: pd.DataFrame):
        # Calculate the short term EMA
        short_ema = prices.close.ewm(span=macd_short_span, adjust=False).mean()
        # Calculate the long term EMA
        long_ema = prices.close.ewm(span=macd_long_span, adjust=False).mean()
        # Calculate the MACD line
        macd = short_ema - long_ema
        # Calculate the signal line
        signal = macd.ewm(span=macd_signal_span, adjust=False).mean()

        prices[MACD_LABEL] = macd
        prices[MACD_SIGNAL_LABEL] = signal

    @staticmethod
    def add_ema(prices: pd.DataFrame, period: int):
        ema = prices.close.ewm(span=period, adjust=False).mean()
        label = 'EMA' + str(period)
        prices[label] = ema

    '''
    @method triple_ema Calculate the 3 moving averages, calculate short/fast moving averages
    @param prices (dataFrame)
    @param short_period (int) Begin of the period
    @param mid_period (int) Middle of the period (should stay in between the 2 others periods parameters)
    @param long_period (int) End of the period
    @return
    '''

    @staticmethod
    def add_triple_ema(prices: pd.DataFrame, short_period: int, mid_period: int, long_period: int):
        # Calculate the 3 moving averages
        short_ema = prices.close.ewm(span=short_period, adjust=False).mean()
        mid_ema = prices.close.ewm(span=mid_period, adjust=False).mean()
        long_ema = prices.close.ewm(span=long_period, adjust=False).mean()

        prices['EMA_SHORT'] = short_ema
        prices['EMA_MID'] = mid_ema
        prices['EMA_LONG'] = long_ema

    @staticmethod
    def add_bollinger_band(prices: pd.DataFrame, period: int, factor: float):
        prices[BOLL_MA] = prices['close'].rolling(window=period).mean()

        prices[BOLL_STD] = prices['close'].rolling(window=period).std()

        prices[BOLL_UP] = prices[BOLL_MA] + (prices[BOLL_STD] * factor)
        prices[BOLL_DOWN] = prices[BOLL_MA] - (prices[BOLL_STD] * factor)

    @staticmethod
    def add_stochastic(prices: pd.DataFrame, k: int, d: int, smooth: int):
        # Create the Lx column
        prices['L' + str(k)] = prices['low'].rolling(window=k).min()

        # Create the Hx column
        prices['H' + str(k)] = prices['high'].rolling(window=k).max()

        # Create the %K column
        prices[STOCHASTIC_K_LABEL] = 100 * ((prices['close'] - prices['L' + str(k)])
                                            / (prices['H' + str(k)] - prices['L' + str(k)]))

        # Create the %D column
        prices[STOCHASTIC_D_LABEL] = prices[STOCHASTIC_K_LABEL].rolling(window=d).mean()

        prices[STOCHASTIC_K_LABEL] = prices[STOCHASTIC_K_LABEL].rolling(window=smooth).mean()

        prices[STOCHASTIC_D_LABEL] = prices[STOCHASTIC_D_LABEL].rolling(window=smooth).mean()

        prices[STOCHASTIC_HIGH_LIMIT_LABEL] = [STOCH_HIGH] * len(prices['close'])
        prices[STOCHASTIC_LOW_LIMIT_LABEL] = [STOCH_LOW] * len(prices['close'])

    @staticmethod
    def add_rsi(prices: pd.DataFrame, period: int):

        upPrices = []
        downPrices = []
        # Loop to hold up and down prices movement
        for i in range(len(prices['close'])):
            if i == 0:
                upPrices.append(0)
                downPrices.append(0)
            else:
                diff = float(prices['close'][i]) - float(prices['close'][i - 1])
                if diff > 0:
                    upPrices.append(diff)
                    downPrices.append(0)
                else:
                    upPrices.append(0)
                    downPrices.append(diff)

        avg_gain = [0.] * period
        avg_loss = [0.] * period

        # Loop to compute the avg gain/loss
        for i in range(period, len(upPrices)):
            sum_gain = 0
            sum_loss = 0
            for y in range(i - period, i):
                sum_gain += upPrices[y]
                sum_loss += downPrices[y]
            avg_gain.append(sum_gain / period)
            avg_loss.append(abs(sum_loss / period))

        rsi = [.0] * period
        # Loop to calculate RSI
        for i in range(period, len(prices['close'])):
            if avg_loss[i] == .0:
                rsi.append(nan)
            else:
                rs_value = (avg_gain[i] / avg_loss[i])
                rsi.append(100 - (100 / (1 + rs_value)))

        prices[RSI_LABEL] = rsi
        prices[RSI30_LABEL] = [RSI_LOWER_LIMIT] * len(prices['close'])
        prices[RSI70_LABEL] = [RSI_UPPER_LIMIT] * len(prices['close'])
