import traceback

from datetime import datetime
from os import mkdir

LOG_DIR = "./data/"
LOG_FILE = "merlin.log"
TRD_FILE = "trades.log"
SGN_FILE = "signals.log"
ERR_FILE = "merlin.err"

PRINT_COLOR_BLUE = '\033[94m'
PRINT_COLOR_GREEN = '\033[92m'
PRINT_COLOR_WARNING = '\033[93m'
PRINT_COLOR_FAIL = '\033[91m'
PRINT_COLOR_ENDC = '\033[0m'


class Log:
    @staticmethod
    def info(data: str):
        try:
            # Retrieve current time
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S%z")

            # Building log line
            data = current_time + " : " + str(data) + "\n"

            # printing to console
            print(f"{PRINT_COLOR_BLUE}INFO - {PRINT_COLOR_ENDC} " + str(data))

            # Printing to the log file
            with open(LOG_DIR + LOG_FILE, "a") as logs:
                logs.write(str(data))
                logs.close()
        # Case log dir doesn't exists
        except FileNotFoundError as e:
            mkdir("./data")
            with open(LOG_DIR + LOG_FILE, "w") as logs:
                logs.write(str(data))
                logs.close()

    @staticmethod
    def error(data: str):
        try:
            # Retrieve current time
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S%z")

            # Building log line
            data = current_time + " : " + str(data) + "\n"

            # Printing to console
            print(f"{PRINT_COLOR_FAIL}ERROR - {PRINT_COLOR_ENDC} " + str(data))

            # Printing to the error file
            with open(LOG_DIR + ERR_FILE, "a") as logs:
                logs.write(str(data))
                traceback.print_exc(file=logs)
                logs.close()
        # Case log dir doesn't exists
        except FileNotFoundError as e:
            mkdir("./data")
            with open(LOG_DIR + ERR_FILE, "w") as logs:
                logs.write(str(data))
                traceback.print_exc(file=logs)
                logs.close()

    @staticmethod
    def trade(trade):
        try:
            # Retrieve current time
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S%z")

            # Building log line
            data = current_time + " : " + str(trade) + "\n"

            # Printing to console
            print(f"{PRINT_COLOR_WARNING}TRADE - {PRINT_COLOR_ENDC} " + str(data))

            # Printing to the error file
            with open(LOG_DIR + TRD_FILE, "a") as logs:
                logs.write(str(data))
                logs.close()
        # Case log dir doesn't exists
        except FileNotFoundError as e:
            mkdir("./data")
            with open(LOG_DIR + TRD_FILE, "w") as logs:
                logs.write(str(data))
                logs.close()

    @staticmethod
    def signal(text: str):
        try:
            # Retrieve current time
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S%z")

            # Building log line
            data = current_time + " : " + str(text) + "\n"

            # Printing to console
            print(f"{PRINT_COLOR_GREEN}SIGNAL - {PRINT_COLOR_ENDC} " + str(data))

            # Printing to the error file
            with open(LOG_DIR + SGN_FILE, "a") as logs:
                logs.write(str(data))
                logs.close()
        # Case log dir doesn't exists
        except FileNotFoundError as e:
            mkdir("./data")
            with open(LOG_DIR + SGN_FILE, "w") as logs:
                logs.write(str(data))
                logs.close()

