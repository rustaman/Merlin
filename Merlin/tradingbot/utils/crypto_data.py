import pandas as pd
import numpy as np
from datetime import datetime, timezone
import csv

from tradingbot.exchange.enums import *
from tradingbot.logic.params import *


class CryptoDataHandler:

    @staticmethod
    def csv_to_dataframe(path: str, delimiter: str, interval=1) -> pd.DataFrame:
        with open(path, "r") as csv_file:
            reader = csv.reader(csv_file, delimiter=delimiter)
            reader.__next__()
            data_dict = {
                'close_time': [],
                'open': [],
                'high': [],
                'low': [],
                'close': [],
            }
            iteration = -1
            for row in reader:
                iteration += 1
                if iteration % interval != 0:
                    continue
                # convert timestamp to datetime
                timestamp = int(row[0]) / 1000
                date = datetime.utcfromtimestamp(timestamp)
                date.replace(tzinfo=timezone.utc)
                data_dict['close_time'].append(date)
                data_dict['open'].append(float(row[3]))
                data_dict['high'].append(float(row[4]))
                data_dict['low'].append(float(row[5]))
                data_dict['close'].append(float(row[6]))

            df = pd.DataFrame.from_dict(data_dict)
            df = df.set_index(pd.DatetimeIndex(df["close_time"].values))

            # Filling the buying and selling signals
            df[BUY_SIGNAL_LABEL] = [np.nan] * len(df["close"])
            df[SELL_SIGNAL_LABEL] = [np.nan] * len(df["close"])
            df[OPENING_BUY_TRADE_LABEL] = [np.nan] * len(df["close"])
            df[OPENING_SELL_TRADE_LABEL] = [np.nan] * len(df["close"])
            df[CLOSING_BUY_TRADE_LABEL] = [np.nan] * len(df["close"])
            df[CLOSING_SELL_TRADE_LABEL] = [np.nan] * len(df["close"])

            df = df.iloc[::-1]

            return df
