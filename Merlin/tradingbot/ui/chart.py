import pandas as pd
import mplfinance as mpf

from tradingbot.logic.strategy import STR
from tradingbot.logic.params import *
from tradingbot.exchange.enums import *


class Chart:

    @staticmethod
    def display_prices_and_indicators(prices: pd.DataFrame, strategy: STR, display_indicators=False):
        # prices = prices.set_index(pd.DatetimeIndex(prices['close_time'].values))
        add_plots = []
        # add emas signals
        for column in prices:
            if str(column).startswith("EMA"):
                plot = mpf.make_addplot(prices[column], type='line', alpha=1, panel=0)
                add_plots.append(plot)

        if strategy == STR.MACD and display_indicators:
            macd_plt = mpf.make_addplot(prices[MACD_LABEL], type='line', color='blue', alpha=1, markersize=24, panel=1)
            sig_plt = mpf.make_addplot(prices[MACD_SIGNAL_LABEL], type='line', color='orange', alpha=1, markersize=24, panel=1)
            add_plots.append(macd_plt)
            add_plots.append(sig_plt)

        elif strategy == STR.BOLL and display_indicators:
            # Boll part
            boll_ma_plt = mpf.make_addplot(prices[BOLL_MA], type='line', color='blue', alpha=0.5, markersize=6, panel=0)
            boll_up_plt = mpf.make_addplot(prices[BOLL_UP], type='line', color='black', alpha=1, markersize=24, panel=0)
            boll_down_plt = mpf.make_addplot(prices[BOLL_DOWN], type='line', color='black', alpha=1, markersize=24, panel=0)
            add_plots.append(boll_ma_plt)
            add_plots.append(boll_up_plt)
            add_plots.append(boll_down_plt)

            # Stochastic part
            stoch_k_plt = mpf.make_addplot(prices[STOCHASTIC_K_LABEL], type='line', linestyle="dashdot", color='#ff5733',
                                           alpha=1, markersize=6, panel=1)
            stoch_d_plt = mpf.make_addplot(prices[STOCHASTIC_D_LABEL], type='line', linestyle="dashdot", color='blue',
                                           alpha=1, markersize=6, panel=1)
            stoch_high_plt = mpf.make_addplot(prices[STOCHASTIC_HIGH_LIMIT_LABEL], type='line', linestyle="dashdot",
                                              color='#2994ff', alpha=1, markersize=6, panel=1)
            stoch_low_plt = mpf.make_addplot(prices[STOCHASTIC_LOW_LIMIT_LABEL], type='line', linestyle="dashdot",
                                             color='#2994ff', alpha=1, markersize=6, panel=1)
            add_plots.append(stoch_k_plt)
            add_plots.append(stoch_d_plt)
            add_plots.append(stoch_high_plt)
            add_plots.append(stoch_low_plt)

            # RSI part
            # rsi_up_plt = mpf.make_addplot(prices[RSI70_LABEL], type='line', linestyle="dashdot", color='purple', alpha=1, markersize=24, panel=2)
            # rsi_down_plt = mpf.make_addplot(prices[RSI30_LABEL], type='line', linestyle="dashdot", color='purple', alpha=1, markersize=24, panel=2)
            # rsi_plt = mpf.make_addplot(prices[RSI_LABEL], type='line', linestyle="dashdot", color='cyan', alpha=1, markersize=24, panel=2)
            # add_plots.append(rsi_up_plt)
            # add_plots.append(rsi_down_plt)
            # add_plots.append(rsi_plt)

        elif strategy == STR.STOCH and display_indicators:

            # Stochastic part
            stoch_k_plt = mpf.make_addplot(prices[STOCHASTIC_K_LABEL], type='line', linestyle="dashdot", color='#ff5733',
                                           alpha=1, markersize=6, panel=1)
            stoch_d_plt = mpf.make_addplot(prices[STOCHASTIC_D_LABEL], type='line', linestyle="dashdot", color='blue',
                                           alpha=1, markersize=6, panel=1)
            stoch_high_plt = mpf.make_addplot(prices[STOCHASTIC_HIGH_LIMIT_LABEL], type='line', linestyle="dashdot",
                                              color='#2994ff', alpha=1, markersize=6, panel=1)
            stoch_low_plt = mpf.make_addplot(prices[STOCHASTIC_LOW_LIMIT_LABEL], type='line', linestyle="dashdot",
                                             color='#2994ff', alpha=1, markersize=6, panel=1)
            add_plots.append(stoch_k_plt)
            add_plots.append(stoch_d_plt)
            add_plots.append(stoch_high_plt)
            add_plots.append(stoch_low_plt)

        # add buy and sell signal to the plots
        # buy_plt = mpf.make_addplot(prices[BUY_SIGNAL_LABEL], type='scatter', marker='^', color='green', alpha=1, markersize=24, panel=0)
        # sell_plt = mpf.make_addplot(prices[SELL_SIGNAL_LABEL], type='scatter', marker='v', color='red', alpha=1, markersize=24, panel=0)
        # add_plots.append(buy_plt)
        # add_plots.append(sell_plt)

        open_buy_trade_plt = mpf.make_addplot(prices[OPENING_BUY_TRADE_LABEL], type='scatter', marker='^',
                                              color='green', alpha=1, markersize=24, panel=0)
        add_plots.append(open_buy_trade_plt)
        #
        # open_sell_trade_plt = mpf.make_addplot(prices[OPENING_SELL_TRADE_LABEL], type='scatter', marker='v',
        #                                         color='red', alpha=1, markersize=24, panel=0)
        # add_plots.append(open_sell_trade_plt)
        #
        # close_buy_trade_plt = mpf.make_addplot(prices[CLOSING_BUY_TRADE_LABEL], type='scatter', marker='^',
        #                                          color='blue', alpha=1, markersize=24, panel=0)
        # add_plots.append(close_buy_trade_plt)
        #
        close_sell_trade_plt = mpf.make_addplot(prices[CLOSING_SELL_TRADE_LABEL], type='scatter', marker='v',
                                                color='orange', alpha=1, markersize=24, panel=0)
        add_plots.append(close_sell_trade_plt)

        # display chart
        mpf.plot(prices, type='candle', addplot=add_plots, style='binance', main_panel=0)
