from time import sleep

from tradingbot.orders.enums import *
from tradingbot.logic.strategy import *
from tradingbot.ui.chart import Chart
from tradingbot.exchange.exchange import Exchange
from tradingbot.exchange.enums import *
from tradingbot.utils.logging import Log
from tradingbot.utils.crypto_data import CryptoDataHandler

from tradingbot.logic.params import SELL_SIGNAL_LABEL
from tradingbot.logic.params import BUY_SIGNAL_LABEL

PRICES_INTERVAL = ITRVL_3M


class Tradebot:
    _risk_tolerance = .01

    def __init__(self, exchange: Exchange, symbol: tuple, risk_tolerance: float, strategy: str, price_nb: int,
                 test_mode=False, live=False):

        self._test_mode = test_mode
        self._live = live
        self._price_nb = price_nb
        self._exchange = exchange
        self._risk_tolerance = risk_tolerance
        self._symbol = symbol
        self.strategy = self._create_strategy(strategy)

    def perform_lap(self):
        # Append last price to prices dataframe stored in the strategy object
        self.strategy.append_last_price(self._exchange.get_symbol_last_price(self._symbol, PRICES_INTERVAL))

        # Computing and adding the buy and sell signals to the prices dataframe stored in the strategy object
        self.strategy.get_buy_sell_signals()

        # Retrieve the last_price from the prices dataframe stored in the strategy object
        last_price = self.strategy.last_price

        # If we hit a buy or sell signal, we then perform a (fake) trade
        if last_price[BUY_SIGNAL_LABEL] == last_price['close']:
            if self._test_mode:
                self._exchange.create_fake_order(SIDE_BUY, self._risk_tolerance, self.strategy.prices, last_price)
            else:
                Log.info("Hit buy signal")
                self._exchange.create_order(SIDE_BUY, self._risk_tolerance, self.strategy.prices)

        elif last_price[SELL_SIGNAL_LABEL] == last_price['close']:
            if self._test_mode:
                self._exchange.create_fake_order(SIDE_SELL, self._risk_tolerance, self.strategy.prices, last_price)
            else:
                Log.info("Hit sell signal")
                self._exchange.create_order(SIDE_SELL, self._risk_tolerance, self.strategy.prices)

        if self._test_mode:
            self._exchange.perform_sim(self._symbol, self.strategy.prices, live=True)

        # Logging profit and loss stats
        results = self._exchange.results(self._symbol, self._test_mode)
        results['session_risk_tolerance'] = self._risk_tolerance
        results['session_strategy'] = self.strategy.strategy.value
        Log.info(results)

    def perform_fake_trade_session(self, price_nb):
        # Stopping stop loss thread
        self._exchange.end_session()
        # Filling the prices data
        self.strategy.prices = CryptoDataHandler.csv_to_dataframe("data/Binance_LTCUSDT_minute.csv", ",", interval=3)
        # Computing and adding the buy and sell signals to the prices dataframe stored in the strategy object
        self.strategy.get_buy_sell_signals()

        # Perform trading session
        for i in range(1, len(self.strategy.prices) - 1):

            # Retrieve the last_price from the prices dataframe stored in the strategy object
            last_price = self.strategy.prices.iloc[i:i + 1].iloc[-1, :]

            # Check if some trades are good to be closed
            self._exchange.perform_sim(self._symbol, self.strategy.prices.iloc[:i+1])

            # If we hit a buy or sell signal, we then perform a (fake) trade
            if last_price[BUY_SIGNAL_LABEL] == last_price['close']:
                self._exchange.create_fake_order(SIDE_BUY, self._risk_tolerance, self.strategy.prices, last_price)
            elif last_price[SELL_SIGNAL_LABEL] == last_price['close']:
                self._exchange.create_fake_order(SIDE_SELL, self._risk_tolerance, self.strategy.prices, last_price)

        results = self._exchange.results(self._symbol, self._test_mode)
        results['session_risk_tolerance'] = self._risk_tolerance
        results['session_strategy'] = self.strategy.strategy.value
        results['trading_symbol'] = str(self._symbol[0]) + str(self._symbol[1])
        Log.info(results)

    def _create_strategy(self, strategy: str):
        return Strategy(self._exchange.get_symbol_prices(self._symbol, self._price_nb, PRICES_INTERVAL), strategy)

    def wait_next_bar(self):
        while self._exchange.get_symbol_last_price(self._symbol, PRICES_INTERVAL).iloc[-1, :]['close_time'] == \
                self.strategy.last_price['close_time']:
            sleep(5)

    def display_price_chart(self, display_indicators=False):
        self._exchange.add_trades_to_prices_data(self.strategy.prices)
        Chart.display_prices_and_indicators(self.strategy.prices, self.strategy.strategy,
                                            display_indicators=display_indicators)
