class ExchangeFilterException(Exception):
    pass


class ExchangeException(Exception):
    pass


class ConnectionErrorException(Exception):
    pass
