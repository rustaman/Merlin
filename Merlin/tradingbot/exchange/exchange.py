import pandas as pd
import numpy as np
import time
import threading

from os import path
from sys import exc_info

from datetime import datetime
from requests.exceptions import RequestException

from binance.client import Client as BinanceClient
from binance.exceptions import *
from binance.enums import *

from tradingbot.exchange.enums import *
from tradingbot.exchange.exceptions import *
from tradingbot.exchange.session import TradingSession
from tradingbot.orders.enums import *
from tradingbot.orders.trade import Trade
from tradingbot.utils.logging import Log

from tradingbot.logic.params import SELL_SIGNAL_LABEL
from tradingbot.logic.params import BUY_SIGNAL_LABEL

SIM_ASSET = 100

TRADE_TAKE_PROFIT_FACTOR = .01
TRADE_STOP_LOSS_FACTOR = .02

TRADE_FILTER_MAX_ALLOWED_INCREASE = 1.1

DELTA_STOP_LOSS_TRIGGER = 0


class Exchange:

    def __init__(self, exchange_name: str, symbol: tuple, live=False, **kwargs):
        # Initializing attributes
        self._exchange_name = exchange_name
        self._symbol = symbol
        self.client = None

        # Initialize first trade id
        self.first_trade_id = 0

        # If simulation is enabled, declaring the sim asset value
        self.sim_asset = kwargs.get("sim_asset", None)

        # If interval is set
        self._interval = kwargs.get("interval", None)

        # Trading session object declaration
        self._session: TradingSession = TradingSession(
            (self.sim_asset if self.sim_asset is not None else SIM_ASSET))

        # Initializing exchange client
        if exchange_name == EXCHG_BINANCE:
            api_key = kwargs.get("api_key", None)
            api_secret = kwargs.get("api_secret", None)
            if api_key is None:
                raise ExchangeException("Missing api_key parameter to instantiate Binance client.")
            elif api_secret is None:
                raise ExchangeException("Missing api_secret parameter to instantiate Binance client.")

            try:
                self.client = BinanceClient(api_key=api_key, api_secret=api_secret)

            except BinanceAPIException as binance_error:
                self._handle_exchange_error(binance_error)

            except BinanceRequestException as e:
                raise ExchangeException("Error while initiating binance exchange client : " + str(e))

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException("Connection error while initiating binance exchange client : " + str(e))

        # fees attribute
        self._fees = self.get_symbol_fees(self._symbol)

        self._session_end = False
        self._check_stop_loss_thread = self._create_stop_loss_thread()
        self._check_stop_loss_thread.start()

        self._monitoring_thread = self._create_monitoring_thread()
        self._monitoring_thread.start()

    def _create_monitoring_thread(self):
        return threading.Thread(target=self._monitor_threads, daemon=True)

    def _create_stop_loss_thread(self):
        self._session_end = False
        return threading.Thread(target=self._stop_loss_check, daemon=False)

    def end_session(self):
        self._session_end = False

    def _monitor_threads(self):
        Log.info("Starting monitoring thread..")
        while True:
            if not self._check_stop_loss_thread.is_alive() and not self._session_end:
                self._check_stop_loss_thread = self._create_stop_loss_thread()
                self._check_stop_loss_thread.start()

            time.sleep(120.)

    def _stop_loss_check(self):
        Log.info("Starting stop-loss thread..")
        while not self._session_end:
            last_order = self._session.last_order
            last_price: float = self._get_tickers_last_price(self._symbol)
            if last_order is None:
                continue
            if last_order[1] is None:
                try:
                    trade_creation_time = last_order[0].date
                    now_time = datetime.now()
                    delta = now_time - trade_creation_time
                    delta = delta.total_seconds() / 60
                    if delta // .1 == 0:
                        print(delta)

                    if last_price <= float(last_order[0].price) * (1 - TRADE_STOP_LOSS_FACTOR) and delta > DELTA_STOP_LOSS_TRIGGER:
                        closing_trade = Trade(SIDE_SELL, TYPE_MARKET, last_price, last_order[0].quantity,
                                              now_time, last_order[0].trade_id + 1)

                        try:
                            closing_trade = self._match_filters(closing_trade, self._symbol)

                        except ExchangeFilterException:
                            Log.info("Skipping because of not matching filters: " + str(closing_trade))
                            return

                        try:
                            closing_trade_resp = self.client.create_order(symbol=str(self._symbol[0]) + str(self._symbol[1]),
                                                                          side=SIDE_SELL,
                                                                          type=ORDER_TYPE_MARKET,
                                                                          quantity=closing_trade.quantity)

                            closing_trade.trade_id = closing_trade_resp['orderId']
                            closing_trade.quantity = closing_trade_resp['executedQty']

                            self._session.close_order(last_order, closing_trade)

                            Log.trade(closing_trade)

                        except BinanceAPIException as binance_error:
                            Log.info(
                                "Skipping order because of exchange returning the following error: " + str(
                                    binance_error.message))
                            self._handle_exchange_error(binance_error)
                            return

                        except BinanceRequestException as e:
                            raise ExchangeException("Error while sending trade orders to the platform exchange : " + str(e))

                        except (ConnectionError, RequestException, ConnectionResetError) as e:
                            raise ConnectionErrorException(str(e))

                except Exception as e:
                    exc_type, exc_obj, exc_tb = exc_info()
                    fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    Log.error(str(type(e)) + str(e) + '\nTYPE {} IN ({}, LINE {})'.format(exc_type, fname, exc_tb.tb_lineno))

            else:
                left_balance = float(self.get_balance(self._symbol)[self._symbol[0]])
                if left_balance * float(self._get_tickers_last_price(self._symbol)) >= float(
                        self._get_symbol_filter(self._symbol)['MIN_NOTIONAL']['minNotional']):

                    trade = Trade(SIDE_SELL, TYPE_MARKET, last_price, left_balance,
                                  datetime.utcnow(), 0)
                    trade = self._match_filters(trade, self._symbol)

                    try:
                        self.client.create_order(symbol=str(self._symbol[0]) + str(self._symbol[1]),
                                                 side=SIDE_SELL,
                                                 type=ORDER_TYPE_MARKET,
                                                 quantity=trade.quantity)

                    except BinanceAPIException as binance_error:
                        self._handle_exchange_error(binance_error)

                    except (ConnectionError, RequestException, ConnectionResetError, BinanceRequestException):
                        time.sleep(2)
                        continue

                    except Exception as e:
                        exc_type, exc_obj, exc_tb = exc_info()
                        fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        Log.error(str(type(e)) + str(e) + '\nTYPE {} IN ({}, LINE {})'.format(exc_type, fname,
                                                                                              exc_tb.tb_lineno))
            time.sleep(1)

        Log.info("Stopping stop-loss thread..")

    def _interval_from_enum(self, interval: str):
        # Converting the interval constance to match it with the constance used by the exchange API
        if self._exchange_name == EXCHG_BINANCE:
            if interval == ITRVL_1M:
                return KLINE_INTERVAL_1MINUTE
            elif interval == ITRVL_3M:
                return KLINE_INTERVAL_3MINUTE
            elif interval == ITRVL_5M:
                return KLINE_INTERVAL_5MINUTE
            elif interval == ITRVL_15M:
                return KLINE_INTERVAL_15MINUTE
            elif interval == ITRVL_30M:
                return KLINE_INTERVAL_30MINUTE
            elif interval == ITRVL_1H:
                return KLINE_INTERVAL_1HOUR
            elif interval == ITRVL_2H:
                return KLINE_INTERVAL_2HOUR
            elif interval == ITRVL_4H:
                return KLINE_INTERVAL_4HOUR
            elif interval == ITRVL_6H:
                return KLINE_INTERVAL_6HOUR
            elif interval == ITRVL_8H:
                return KLINE_INTERVAL_8HOUR
            elif interval == ITRVL_12H:
                return KLINE_INTERVAL_12HOUR
            elif interval == ITRVL_1D:
                return KLINE_INTERVAL_1DAY
            elif interval == ITRVL_3D:
                return KLINE_INTERVAL_3DAY
            elif interval == ITRVL_1W:
                return KLINE_INTERVAL_1WEEK
            elif interval == ITRVL_1MONTH:
                return KLINE_INTERVAL_1MONTH

        else:
            return None

    def _get_symbol_filter(self, symbol: tuple):
        # Retrieving trade filters depending on the plateform
        filters = {}
        if self._exchange_name == EXCHG_BINANCE:
            symbol = str(symbol[0]) + str(symbol[1])
            try:
                info = self.client.get_symbol_info(symbol)
                for item in info['filters']:
                    filters[item['filterType']] = item

            except BinanceAPIException as binance_error:
                self._handle_exchange_error(binance_error)

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

            except BinanceRequestException as e:
                raise ExchangeException(str(e))

        if filters:
            return filters

        raise ExchangeException("Unable to retrieve trade filters for symbol " + str(symbol) + " on exchange "
                                + self._exchange_name)

    def _match_filters(self, trade: Trade, symbol: tuple):
        filters = self._get_symbol_filter(symbol)

        # If necessary, modifETHying a trade object to make it matching with the exchange order filters
        if self._exchange_name == EXCHG_BINANCE:
            # TODO Add PERCENT_PRICE filter handling
            # TODO Add MAX_NUM_ORDERS filter handling
            # Price filters
            # Checking if price matches the price filters
            if trade.price < float(filters['PRICE_FILTER']['minPrice']):
                # If in order to math trade filter the trade value would be increased by more than 10%
                if (float(filters['PRICE_FILTER']['minPrice']) * trade.quantity) / (
                        trade.price * trade.quantity) > TRADE_FILTER_MAX_ALLOWED_INCREASE:
                    raise ExchangeFilterException("Cannot adjust trade params (price) to match filters, \
                    trade value would exceed 10% of initial value")
                trade.price = float(filters['PRICE_FILTER']['minPrice'])

            elif trade.price > float(filters['PRICE_FILTER']['maxPrice']):
                trade.price = float(filters['PRICE_FILTER']['maxPrice'])

            # Checking if stop_price matches the price filters
            if hasattr(trade, "stop_price"):
                if trade.stop_price < float(filters['PRICE_FILTER']['minPrice']):
                    # If in order to math trade filter the trade value would be increased by more than 10%
                    if (float(filters['PRICE_FILTER']['minPrice']) * trade.quantity) / (
                            trade.stop_price * trade.quantity) > 1.1:
                        raise ExchangeFilterException("Cannot adjust trade params (stop price) to match filters, \
                            trade value would exceed 10% of initial value")
                    trade.stop_price = float(filters['PRICE_FILTER']['minPrice'])

                elif trade.stop_price > float(filters['PRICE_FILTER']['maxPrice']):
                    trade.stop_price = float(filters['PRICE_FILTER']['maxPrice'])

            # Matching the price's precision filter
            # Divide only if tickSize is set
            if float(filters['PRICE_FILTER']['tickSize']) > .0:
                divider = trade.price // float(filters['PRICE_FILTER']['tickSize'])
                trade.price = float(filters['PRICE_FILTER']['tickSize']) * divider  # Add a little precision shift
                precision = len(str(float(filters['PRICE_FILTER']['tickSize'])))
                trade.price = round(trade.price, precision)

                if hasattr(trade, "stop_price"):
                    divider = trade.stop_price // float(filters['PRICE_FILTER']['tickSize'])
                    trade.stop_price = float(
                        filters['PRICE_FILTER']['tickSize']) * divider  # Add a little precision shift
                    trade.stop_price = round(trade.stop_price, precision)

            # Quantity filters
            if trade.quantity == .0:
                raise ExchangeFilterException("Quantity for a trade cannot be 0, maybe there is not enough asset.")

            # Prevent str interpretation error for trade quantity attribute
            trade.quantity = float(trade.quantity)

            if trade.quantity < float(filters['LOT_SIZE']['minQty']):
                # If in order to math trade filter the trade value would be increased by more than 10%
                if (float(filters['LOT_SIZE']['minQty']) * trade.price) / (
                        trade.price * trade.quantity) > TRADE_FILTER_MAX_ALLOWED_INCREASE:
                    raise ExchangeFilterException("Cannot adjust trade params (quantity) to match filters, \
                        trade value would exceed 10% of initial value")
                trade.quantity = float(filters['LOT_SIZE']['minQty'])

            elif trade.quantity > float(filters['LOT_SIZE']['maxQty']):
                trade.quantity = float(filters['LOT_SIZE']['maxQty'])

            # Matching the quantity's precision filter
            divider = trade.quantity // float(filters['LOT_SIZE']['stepSize'])
            trade.quantity = float(filters['LOT_SIZE']['stepSize']) * divider  # Add a little precision shift
            precision = len(str(float(filters['LOT_SIZE']['stepSize'])))
            trade.quantity = round(trade.quantity, precision)

            if trade.trade_type == TYPE_MARKET:
                if trade.quantity < float(filters['MARKET_LOT_SIZE']['minQty']):
                    # If in order to math trade filter the trade value would be increased by more than 10%
                    if (float(filters['MARKET_LOT_SIZE']['minQty']) * trade.price) / (
                            trade.price * trade.quantity) > TRADE_FILTER_MAX_ALLOWED_INCREASE:
                        raise ExchangeFilterException("Cannot adjust trade params (quantity) to match filters, \
                                                trade value would exceed 10% of initial value")
                    trade.quantity = float(filters['MARKET_LOT_SIZE']['minQty'])
                elif trade.quantity > float(filters['MARKET_LOT_SIZE']['maxQty']):
                    trade.quantity = float(filters['MARKET_LOT_SIZE']['maxQty'])

                # Matching the quantity's precision filter
                # Divide only if stepSize is set
                if float(filters['MARKET_LOT_SIZE']['stepSize']) > .0:
                    divider = trade.quantity // float(filters['LOT_SIZE']['stepSize'])
                    trade.quantity = float(
                        filters['MARKET_LOT_SIZE']['stepSize']) * divider  # Add a little precision shift
                    precision = len(str(float(filters['MARKET_LOT_SIZE']['stepSize'])))
                    trade.quantity = round(trade.quantity, precision)
                else:
                    precision = len(str(float(filters['LOT_SIZE']['stepSize'])))
                    trade.quantity = round(trade.quantity, precision)

            # Checking MIN_NOTIONAL filter
            if trade.price * trade.quantity < float(filters['MIN_NOTIONAL']['minNotional']):
                # Raising exception if trade not matching those filters For MARKET trades, depends if applyToMarket
                # is True. Only check for MARKET trades because a stop-limit trade only comes after a market so the
                # quantity is available
                if trade.trade_type == TYPE_MARKET and bool(filters['MIN_NOTIONAL']['applyToMarket']):
                    raise ExchangeFilterException(
                        "The order filter MIN_NOTIONAL (price x quantity) from the " + self._exchange_name +
                        " exchange cannot be satisfied.")

            # Checking maximal open orders

        return trade

    def _is_last_order_closed(self, symbol: tuple, side: str):
        try:
            last_order = self._get_opened_orders()[-1]

            if self._exchange_name == EXCHG_BINANCE:
                if last_order['side'] == side:
                    return False
                else:
                    return True

        except IndexError:
            return True

    def _get_tickers_last_price(self, symbol: tuple):
        last_price: float = .0
        if self._exchange_name == EXCHG_BINANCE:
            try:
                str_symbol = str(symbol[0]) + str(symbol[1])
                last_price = float(self.client.get_ticker(symbol=str_symbol)['lastPrice'])

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

            except BinanceAPIException as binance_error:
                self._handle_exchange_error(binance_error)

            except BinanceRequestException as e:
                raise ExchangeException(str(e))

        return last_price

    def _get_trade_by_id(self, trade_id):
        orders = self._get_all_orders()
        if self._exchange_name == EXCHG_BINANCE:
            for order in orders:
                if int(order['orderId']) == trade_id:
                    return order
            return None

    def _cancel_trade(self, trade_id):
        if self._exchange_name == EXCHG_BINANCE:
            try:
                str_symbol = str(self._symbol[0]) + str(self._symbol[1])
                self.client.cancel_order(symbol=str_symbol, orderId=trade_id)
                trade = self._get_trade_by_id(trade_id)
                if trade is not None:
                    Log.info("Cancelling trade : " + str(trade))

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

            except BinanceAPIException as binance_error:
                self._handle_exchange_error(binance_error)

            except BinanceRequestException as e:
                raise ExchangeException(str(e))

    def get_symbol_last_price(self, symbol: tuple, interval: str):
        interval = self._interval_from_enum(interval)
        price = {}

        try:
            # Declaring the price dict
            price_dict = {'open_time': [],
                          'open': [],
                          'high': [],
                          'low': [],
                          'close': [],
                          'volume': [],
                          'close_time': []}

            # Retrieving and filling the price depending on the exchange
            if self._exchange_name == EXCHG_BINANCE:
                symbol = str(symbol[0]) + str(symbol[1])
                price = self.client.get_klines(symbol=symbol, interval=interval, limit=1)[0]

                open_time_dt = datetime.fromtimestamp(int(price[0]) / 1000)
                open_time_dt = open_time_dt.replace(microsecond=0)

                close_time_dt = datetime.fromtimestamp(int(price[0]) / 1000)
                close_time_dt = close_time_dt.replace(microsecond=0)

                price_dict['open_time'] = [open_time_dt]  # Millis to second
                price_dict['open'] = [float(price[1])]
                price_dict['high'] = [float(price[2])]
                price_dict['low'] = [float(price[3])]
                price_dict['close'] = [float(price[4])]
                price_dict['volume'] = [float(price[5])]
                price_dict['close_time'] = [close_time_dt]

            # Setting the dataframe index
            price = pd.DataFrame.from_dict(price_dict)
            price = price.set_index(pd.DatetimeIndex(price['close_time'].values))

            # Filling the buying and selling signals
            price[BUY_SIGNAL_LABEL] = [np.nan]
            price[SELL_SIGNAL_LABEL] = [np.nan]
            price[OPENING_BUY_TRADE_LABEL] = [np.nan]
            price[OPENING_SELL_TRADE_LABEL] = [np.nan]
            price[CLOSING_BUY_TRADE_LABEL] = [np.nan]
            price[CLOSING_SELL_TRADE_LABEL] = [np.nan]

        except (ConnectionError, RequestException, ConnectionResetError) as e:
            raise ConnectionErrorException(str(e))

        except BinanceAPIException as binance_error:
            self._handle_exchange_error(binance_error)

        except BinanceRequestException as e:
            raise ExchangeException(str(e))

        return price

    def get_symbol_prices(self, symbol: tuple, period: int, interval: str):
        interval = self._interval_from_enum(interval)

        try:
            # Declaring the prices dict
            prices_dict = {'open_time': [],
                           'open': [],
                           'high': [],
                           'low': [],
                           'close': [],
                           'volume': [],
                           'close_time': []}

            # Filling prices dict depending on the exchange
            if self._exchange_name == EXCHG_BINANCE:
                symbol = str(symbol[0]) + str(symbol[1])
                prices = self.client.get_klines(symbol=symbol, interval=interval, limit=period)

                for price in prices:
                    open_time_dt = datetime.fromtimestamp(int(price[0]) / 1000)
                    open_time_dt = open_time_dt.replace(microsecond=0)

                    close_time_dt = datetime.fromtimestamp(int(price[0]) / 1000)
                    close_time_dt = close_time_dt.replace(microsecond=0)

                    prices_dict['open_time'] += [open_time_dt]  # from millis to seconds
                    prices_dict['open'] += [float(price[1])]
                    prices_dict['high'] += [float(price[2])]
                    prices_dict['low'] += [float(price[3])]
                    prices_dict['close'] += [float(price[4])]
                    prices_dict['volume'] += [float(price[5])]
                    prices_dict['close_time'] += [close_time_dt]

            # Setting the dataframe index
            df = pd.DataFrame.from_dict(prices_dict)
            df = df.set_index(pd.DatetimeIndex(df['close_time'].values))

            # Filling the buying and selling signals
            df[BUY_SIGNAL_LABEL] = [np.nan] * len(df['close'])
            df[SELL_SIGNAL_LABEL] = [np.nan] * len(df['close'])
            df[OPENING_BUY_TRADE_LABEL] = [np.nan] * len(df['close'])
            df[OPENING_SELL_TRADE_LABEL] = [np.nan] * len(df['close'])
            df[CLOSING_BUY_TRADE_LABEL] = [np.nan] * len(df['close'])
            df[CLOSING_SELL_TRADE_LABEL] = [np.nan] * len(df['close'])

            return df

        except BinanceAPIException as binance_error:
            self._handle_exchange_error(binance_error)

        except BinanceRequestException as e:
            raise ExchangeException(str(e))

        except (ConnectionError, RequestException, ConnectionResetError) as e:
            raise ConnectionErrorException(str(e))

    def get_symbol_fees(self, symbol: tuple):
        res = {'buy': .0,
               'sell': .0}
        # Retrieving buy and sell fees depending on the exchange
        if self._exchange_name == EXCHG_BINANCE:
            try:
                symbol = str(symbol[0]) + str(symbol[1])
                fees = self.client.get_trade_fee()
                if 'tradeFee' in fees:
                    fees = fees['tradeFee']
                else:
                    return None

                for fee in fees:
                    if fee['symbol'] == symbol:
                        res['buy'] = fee['taker']
                        res['sell'] = fee['maker']

            except BinanceAPIException as binance_error:
                # self._handle_exchange_error(binance_error)
                # TODO uncomment line above
                pass
            except BinanceRequestException as e:
                raise ExchangeException(str(e))

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

        return res

    def get_balance(self, symbol: tuple):
        balances = {}
        # Retrieving symbol balances depending the exchange
        if self._exchange_name == EXCHG_BINANCE:
            try:
                assets = self.client.get_account()
                if 'balances' in assets:
                    assets = assets['balances']
                else:
                    raise ExchangeException("Unable to retrieve asset info from the exchange")
                for asset in assets:
                    if 'asset' in asset:
                        if asset['asset'] == symbol[0] or asset['asset'] == symbol[1]:
                            balances[asset['asset']] = float(asset['free'])

            except BinanceAPIException as binance_error:
                self._handle_exchange_error(binance_error)

            except BinanceRequestException as e:
                raise ExchangeException(str(e))

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

        if len(balances) < 2:
            raise ExchangeException("Unable to retrieve both balances for the provided symbol : "
                                    + str(symbol[0]) + str(symbol[1]) + " on exchange "
                                    + self._exchange_name)
        return balances

    def perform_sim(self, symbol: tuple, prices: pd.DataFrame, live=False):
        """
        Performs al the simulation action related to fake trading
        :param symbol: Symbol of the current trading session
        :param prices: Prices dataframe (Can be obtained using method get_symbol_prices from the Exchange class)
        :param live: Set to true if it's a live trading session whether it is a simulation or not
        :return: N/A
        """
        if live:
            self._session.close_trade_matching_price(prices, DELTA_STOP_LOSS_TRIGGER, TRADE_STOP_LOSS_FACTOR, live=live)
        else:
            self._session.close_trade_matching_price(prices, DELTA_STOP_LOSS_TRIGGER, TRADE_STOP_LOSS_FACTOR, live=live)

    def results(self, symbol: tuple, test_mode: bool):
        if not test_mode:
            if self._exchange_name == EXCHG_BINANCE:
                orders = self._get_all_orders()
                for order in orders:
                    if order['status'] == "FILLED" and order['type'] == "LIMIT":
                        saved_order = self._session.get_order_by_id(order['orderId'])
                        if saved_order is not None:
                            saved_order[1].close(
                                float(order['price']) if float(order['price']) > 0.0 else float(
                                    order['stopPrice'])), datetime.fromtimestamp(int(order['updateTime']) / 1000)
            results = self._session.results(self.get_symbol_fees(self._symbol))
            assets = self.get_balance(symbol)
            results['remain_asset'] = assets
            return results

        else:
            results = self._session.results(self.get_symbol_fees(self._symbol))
            results['remain_asset'] = self._session.asset + results['net_profit_loss']
            return results

    def add_trades_to_prices_data(self, prices: pd.DataFrame):
        orders = self._session.orders
        for order in orders:
            if order[1] is not None:
                stats_trade_column = CLOSING_SELL_TRADE_LABEL if order[1].side == SIDE_SELL else CLOSING_BUY_TRADE_LABEL
                index = order[1].closing_time
                prices.at[index, stats_trade_column] = order[1].closing_price

    def create_fake_order(self, side: str, risk_tolerance: float, prices: pd.DataFrame, last_price: pd.DataFrame):
        """
        Create a fake order for the fake trading simulation session
        :param side: Side of the order SIDE_BUY or SIDE_SELL (enums from tradingbot.orders.enums.py)
        :param risk_tolerance: Trading session risk tolerance (from 0.01 to 1.0)
        :param prices: Prices dataframe (Can be obtained using method get_symbol_prices from Exchange class)
        :param last_price: last_price used in not live simulation trading session
        :return: N/A
        """

        # Retrieve last price if passed parameter is a list
        if isinstance(last_price, pd.DataFrame):
            last_price = last_price.iloc[-1, :]

        # Retrieve last trade id
        last_id: int = self._session.last_order_id

        # Declaring trades that composing the new order
        opening_trade = None
        closing_trade = None

        # Create the trades composing the order depending on the trade side
        if side == SIDE_BUY:
            # Preventing from create order if last one is still open
            if not self._session.is_last_order_closed():
                return

            # Computing order quantity parameter
            available: float = self._session.asset * risk_tolerance
            quantity: float = available / last_price['close']

            opening_trade = Trade(SIDE_BUY, TYPE_MARKET, last_price['close'], quantity,
                                  last_price['close_time'], last_id + 1)

            self._session.add_order(opening_trade)

            index = opening_trade.closing_time
            prices.at[index, OPENING_BUY_TRADE_LABEL] = opening_trade.closing_price

            #Log.trade(opening_trade)

        elif side == SIDE_SELL:
            if self._session.is_last_order_closed():
                return
            last_order = self._session.last_order
            closing_trade = Trade(SIDE_SELL, TYPE_MARKET, last_price['close'], last_order[0].quantity,
                                  last_price['close_time'], last_order[0].trade_id + 1)
            self._session.close_order(last_order, closing_trade)

            index = closing_trade.closing_time
            prices.at[index, CLOSING_SELL_TRADE_LABEL] = closing_trade.closing_price

            #Log.trade(closing_trade)

    def create_order(self, side: str, risk_tolerance: float, prices: pd.DataFrame):
        """
        Create an order and send it to the exchange platform
        :param side: Side of the order SIDE_BUY or SIDE_SELL (enums from tradingbot.orders.enums.py)
        :param risk_tolerance: Trading session risk tolerance (from 0.001 to 1.0)
        :param prices: Prices dataframe (Can be obtained using method get_symbol_prices from Exchange class)
        :return: N/A
        """

        # Instantiating sim object if not already done
        if self._session is None:
            self._session = TradingSession((self.sim_asset if self.sim_asset is not None else SIM_ASSET))

        # Retrieve last price
        last_price = prices.iloc[-1, :]

        # Retrieving symbol balance
        assets = self.get_balance(self._symbol)

        # Concatenating symbol
        str_symbol = str(self._symbol[0]) + str(self._symbol[1])

        # Create the trades composing the order depending on the trade side
        if side == SIDE_BUY:
            # Preventing from create order if last one is still open
            if not self._session.is_last_order_closed():
                return

            # Computing order quantity parameter
            available: float = float(assets[self._symbol[1]]) * risk_tolerance
            quantity: float = available / float(last_price['close'])

            # Creating the trades
            opening_trade = Trade(SIDE_BUY, TYPE_MARKET, float(last_price['close']), float(quantity),
                                  last_price['close_time'], 0)

            try:
                opening_trade = self._match_filters(opening_trade, self._symbol)
            except ExchangeFilterException:
                Log.info("Skipping because of not matching filters: " + str(opening_trade))
                return

            try:
                opening_trade_resp = self.client.create_order(symbol=str_symbol,
                                                              side=SIDE_BUY,
                                                              type=ORDER_TYPE_MARKET,
                                                              quantity=opening_trade.quantity)

                opening_trade.quantity = float(opening_trade_resp['executedQty'])
                opening_trade.trade_id = opening_trade_resp['orderId']

                self._session.add_order(opening_trade)

                Log.trade(opening_trade)

            except BinanceAPIException as binance_error:
                Log.info(
                    "Skipping order because of exchange returning the following error: " + str(binance_error.message))
                self._handle_exchange_error(binance_error)
                return

            except BinanceRequestException as e:
                raise ExchangeException("Error while sending trade orders to the platform exchange : " + str(e))

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

        elif side == SIDE_SELL:
            if self._session.is_last_order_closed():
                return

            last_order = self._session.last_order

            closing_trade = Trade(SIDE_SELL, TYPE_MARKET, float(last_price['close']), float(last_order[0].quantity),
                                  last_price['close_time'], last_order[0].trade_id + 1)

            try:
                closing_trade = self._match_filters(closing_trade, self._symbol)

            except ExchangeFilterException:
                Log.info("Skipping because of not matching filters: " + str(closing_trade))
                return

            try:
                closing_trade_resp = self.client.create_order(symbol=str_symbol,
                                                              side=SIDE_SELL,
                                                              type=ORDER_TYPE_MARKET,
                                                              quantity=closing_trade.quantity)

                closing_trade.trade_id = closing_trade_resp['orderId']
                closing_trade.quantity = float(closing_trade_resp['executedQty'])

                self._session.close_order(last_order, closing_trade)

                Log.trade(closing_trade)

            except BinanceAPIException as binance_error:
                Log.info(
                    "Skipping order because of exchange returning the following error: " + str(binance_error.message))
                self._handle_exchange_error(binance_error)
                return

            except BinanceRequestException as e:
                raise ExchangeException("Error while sending trade orders to the platform exchange : " + str(e))

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

    def _get_all_orders(self):
        orders = []
        if self._exchange_name == EXCHG_BINANCE:
            try:
                str_symbol = str(self._symbol[0]) + str(self._symbol[1])
                orders = self.client.get_all_orders(symbol=str_symbol)

            except (ConnectionError, RequestException, ConnectionResetError) as e:
                raise ConnectionErrorException(str(e))

            except BinanceAPIException as binance_error:
                self._handle_exchange_error(binance_error)

            except BinanceRequestException as e:
                raise ExchangeException(str(e))

        return orders

    def _get_opened_orders(self):
        res = []
        if self._exchange_name == EXCHG_BINANCE:
            orders = self._get_all_orders()
            for order in orders:
                if order['status'] == "NEW":
                    res.append(order)

        return res

    def _handle_exchange_error(self, error: Exception):
        if self._exchange_name == EXCHG_BINANCE:
            if hasattr(error, "code"):
                # Request issues
                if 1100 <= error.code < 2000:
                    raise ExchangeException("Error with request: " + str(error))
