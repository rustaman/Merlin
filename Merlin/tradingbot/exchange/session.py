import pandas as pd

from typing import Dict
from datetime import datetime

from tradingbot.orders.trade import Trade
from tradingbot.orders.enums import *
from tradingbot.utils.logging import Log


class TradingSession:

    def __init__(self, asset):
        self._orders = []
        self._asset = asset

    @property
    def orders(self):
        return self._orders

    @property
    def last_order_id(self):
        """
        Return closing trade id from the last order, -1 if there is no order
        :return: Last trade id from the last order, -1 if there is no order
        """
        if len(self._orders) > 0:
            return self._orders[-1][0].trade_id + 1
        else:
            return -1

    def results(self, fees: dict) -> Dict:
        """
        Return the stats results of the fake trading session
        :return: Results as a Dict object
        """
        if len(fees) != 2:
            raise ValueError("Fees must be a two value dict")
        # Initiating the results variable
        results = {'order_qty': 0,
                   'orders_won': 0,
                   'orders_lost': 0,
                   'order_win_rate': .0,
                   'raw_profit_loss': .0,
                   'net_profit_loss': .0,
                   'fees_qty': .0,
                   'profit': .0,
                   'loss': .0,
                   'remain_asset': 0}

        for order in self._orders:
            if order[0].closed and order[1]:
                # We compute the fess and convert them in 2nd pair value (ex: fees in xrp converted in usdt)
                fees_qty = order[1].quantity * float(fees['sell']) * order[1].closing_price

                # Computing the trades value
                opening_trade_value: float = order[0].closing_price * order[0].quantity
                closing_trade_value: float = order[1].closing_price * order[1].quantity

                order_result = (closing_trade_value - opening_trade_value)

                # Filling the results variable
                results['order_qty'] += 1
                results['raw_profit_loss'] += order_result
                order_result -= fees_qty
                results['net_profit_loss'] += order_result
                results['fees_qty'] += fees_qty
                results['profit'] += order_result if order_result > 0. else 0
                results['loss'] += order_result if order_result < 0. else 0
                results['orders_won'] += 1 if order_result > 0. else 0
                results['orders_lost'] += 1 if order_result < 0. else 0

        # Filling the general fileds of the results variable
        results['order_win_rate'] = (results['orders_won'] / results['order_qty']) if results['order_qty'] > 0.0 else .0
        results['remain_asset'] = self._asset

        return results

    @property
    def asset(self):
        return self._asset

    def set_asset(self, asset):
        self._asset = asset

    @property
    def last_order(self):
        if len(self._orders) > 0:
            return self._orders[-1]
        else:
            return None

    def add_order(self, opening_trade: Trade):
        """
        Add a fake order to the order list
        :param opening_trade: Opening trade of the order
        :param closing_trade: Closing trade of the order
        :return: N/A
        """
        self._orders.append((opening_trade, None))

    def close_trade_matching_price(self, prices: pd.DataFrame, stoploss_delta: int, stop_loss_factor: float,
                                   live=False):
        """
        Close fake orders that match with the price
        :param stoploss_delta:
        :param stop_loss_factor:
        :param live: live trading (True or False)
        :param prices: Prices dataframe object (returned by method get_symbol_prices of class Exchange)
        :return: N/A
        """
        # Retrieve last price and index
        index: datetime = datetime.utcnow()
        last_price = float(prices.iloc[-1, :]['close'])
        high_price = float(prices.iloc[-1, :]['high'])
        low_price = float(prices.iloc[-1, :]['low'])

        if not live:
            index = prices.iloc[-1, :]['close_time']

        for i in range(len(self._orders)):
            # Check only with opened orders
            if self._orders[i][1] is None:
                opening_trade = self._orders[i][0]
                delta = index - opening_trade.date
                delta = delta.total_seconds() / 60

                if opening_trade.price * (1 - stop_loss_factor) >= last_price and delta > stoploss_delta:
                    closing_trade = Trade(SIDE_SELL, TYPE_MARKET, last_price, opening_trade.quantity,
                                                            index, opening_trade.trade_id + 1)
                    self._orders[i] = (opening_trade, closing_trade)

                    # if closing_trade.side == SIDE_BUY:
                    #     if last_price <= closing_trade.price:
                    #         closing_trade.close(closing_trade.price, index)
                    #     elif last_price >= closing_trade.stop_price:
                    #         closing_trade.close(closing_trade.stop_price, index)
                    #
                    # elif closing_trade.side == SIDE_SELL:
                    #     if last_price >= closing_trade.price:
                    #         closing_trade.close(closing_trade.price, index)
                    #     elif last_price <= closing_trade.stop_price:
                    #         closing_trade.close(closing_trade.stop_price, index)

    def get_order_by_id(self, trade_id: int):
        for order in self._orders:
            if order[0].trade_id == trade_id:
                return order

            elif order[1] is not None:
                if order[1].trade_id == trade_id:
                    return order
            else:
                return None

    def is_last_order_closed(self):
        if len(self._orders) > 0:
            return self._orders[-1][1] is not None
        else:
            return True

    def close_order(self, order_to_close, closing_trade: Trade):
        for i in range(len(self._orders)):
            if self._orders[i] == order_to_close:
                self._orders[i] = (order_to_close[0], closing_trade)
