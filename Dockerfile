FROM python:3.7-buster

RUN mkdir /Merlin

COPY Merlin/tradingbot /Merlin/tradingbot

COPY Merlin/merlin.py /Merlin/merlin.py

COPY Merlin/requirements.txt /Merlin/requirements.txt 

WORKDIR /Merlin

RUN pip install -r ./requirements.txt

ENTRYPOINT [ "python", "/Merlin/merlin.py"]
